PROGRAM = check
SOURCE = *.go

build:
	CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static -s"' -o $(PROGRAM) $(SOURCE)

clean:
	rm -f $(PROGRAM)

fmt:
	gofmt -w $(SOURCE)

vet:
	go vet $(SOURCE)

docker:
	docker build -t bp .

tag:
	docker tag bp:latest code.vt.edu:5005/rtilley/bp/$(PROGRAM):latest

push:
	docker push code.vt.edu:5005/rtilley/bp/$(PROGRAM):latest

# echo $gitlab_rw_registry_token | sudo make login
login:
	docker login code.vt.edu:5005 -u rtilley --password-stdin
