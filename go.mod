module bp

go 1.19

require (
	github.com/bits-and-blooms/bloom v2.0.3+incompatible
	github.com/bits-and-blooms/bloom/v3 v3.7.0
	github.com/gorilla/handlers v1.5.1
	github.com/gorilla/mux v1.8.0
)

require (
	github.com/bits-and-blooms/bitset v1.10.0 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/spaolacci/murmur3 v1.1.0 // indirect
	github.com/willf/bitset v1.1.11 // indirect
)
